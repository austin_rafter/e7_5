import java.util.InputMismatchException;
import java.util.Scanner;
/*
*Austin Rafter
* 013433901
* CS 049J
* 9/19/2020
*
* Take user input of integer for array size
* Take user input of integer for array elements
* Calculate alternating sum of all elements in array
* Even positions are odd
* Odd positions are even
 */

public class E7_5 {
    public static void main(String[] args) {

        System.out.println("Enter the size of the array: ");//prompt for array size
        Scanner scanObject = new Scanner(System.in); //scanner object for user input
        int nArraySize = scanObject.nextInt(); //user enters int for array size
        int nArrayWithSize[] = new int[nArraySize]; //set array size to user entered int
        System.out.println("Enter integers for the elements of the array one by one: ");
        //prompt user to enter integers for elements of arrays

        for (int i = 0; i < nArrayWithSize.length; i++) { // loop to go through array
            //int nUserEntryForArray = scanObject.nextInt();
            nArrayWithSize[i] = scanObject.nextInt(); //user enters integers to populate array
            //try {
            //nArrayWithSize[i] = Integer.parseInt(nUserEntryForArray);
            //}
            //catch (NumberFormatException exceptionError) {
            //   System.out.println("Error! Please enter an integer!.");
            // }
            //https://stackoverflow.com/questions/53101257/catching-error-when-a-user-enters-a-string-instead-of-an-integer-input
        }
        int nSecondArrayForRestore[] = new int[nArraySize]; //create second array same size of first array

        for (int i = 0; i < nSecondArrayForRestore.length; i++) { //loop through second array
            if ((i % 2) == 0) { //check if position at array is even, if so keep it positive
                nSecondArrayForRestore[i] = nArrayWithSize[i];
                //store into second array
            } else { //if position is even, set element to new int
                int nEvenArrayIndex = nArrayWithSize[i];
                nSecondArrayForRestore[i] = -nEvenArrayIndex; //set new int to negative, and store into second array
            }
        }
        int nArrayMathTotal = 0; //create int for array total
        for (int i = 0; i < nSecondArrayForRestore.length; i++) { //loop to run through second array
            nArrayMathTotal += nSecondArrayForRestore[i]; //add element to running total

            if (i == (nSecondArrayForRestore.length - 1)) {
                System.out.print(nSecondArrayForRestore[i] + " = " + nArrayMathTotal); //print total and last element
            } else if ((i % 2) == 0) {
                System.out.print(nSecondArrayForRestore[i] + " "); //print even positions
            } else {
                System.out.print(nSecondArrayForRestore[i] + " + "); //print odd positions
            }

        }
    }
}
